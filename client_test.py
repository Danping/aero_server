#!/usr/bin/python2.7
import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connect the socket to the port where the server is listening
    server_address = ('192.168.3.4', 10002)
    print >>sys.stderr, 'connecting to %s port %s' % server_address
    sock.connect(server_address)

    # Send data
    message = '[raw]rosbag record stop\n'
    print >>sys.stderr, 'sending "%s"' % message
    sock.send(message)

    # Look for the response
    amount_received = 0
    amount_expected = len(message)

    while amount_received < amount_expected:
        data = sock.recv(256)
        amount_received += len(data)
        print >>sys.stderr, 'received "%s"' % data
finally:
    raw_input("Press Enter to continue...")
    print >>sys.stderr, 'closing socket'
    sock.close()
