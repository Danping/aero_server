#!/usr/bin/python2.7
import socket
import sys
import signal
import time
import logging
import subprocess
from socket_utils import *

def get_local_ip():
    try:
        # a little trick to get the IP address
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
        s.close()
    except:
        ip = '192.168.8.1'    
    return  ip


def run_raw_command( cmd_str):
    logging.info("[run command]:%s",cmd_str)
    cmd_pipe = subprocess.Popen(cmd_str.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = cmd_pipe.communicate()
    return out,err


def check_roscore():
    #check if the roscore is started
    list_cmd = subprocess.Popen(["rosnode","list"],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = list_cmd.communicate()
    logging.info("retcode '%s', '%s'", out,err)
    if not out and err[0:5]=="ERROR":
        return False
    return True


def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    aero_server._to_quit = True

class AeroServer:
    def __init__(self, port=10000):
        self._port = port
        self._is_recording = False
        self._is_streaming = False
        self._record_topic = ''
        self._pipe_bcam = []
        self._to_quit = False
        self._streaming_str = "rosparam set /camera_streaming/topic_name /camera/color/image_raw"
#	self._streaming_str = "rosparam set /camera_streaming/topic_name /aero/bcam/image"

    def __del__(self):
        if self._pipe_bcam:
            self._pipe_bcam.terminate()

    def start_ros_nodes(self):
	#just for debug
	#self._pipe_bcam = subprocess.Popen("roslaunch gscam v4l.launch".split())
	return 0


    def start(self):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        IP = get_local_ip()
        server_address = (IP, self._port)
        logging.info('starting up on %s port %s', IP, self._port)
        self._sock.bind(server_address)
        #Only allow to connect one client at one time
        self._sock.listen(1)
        signal.signal(signal.SIGINT, signal_handler)
        self.start_ros_nodes()
        while not self._to_quit:
            self.record_bag_off()
            # Wait for a connection
            logging.info('waiting for a connection')
            connection, self._client_address = self._sock.accept()
            connection.settimeout(5)
            try:
                logging.info('connection from %s', self._client_address)
                self.on_new_connection()
                # Receive the data in small chunks and retransmit it
                while True:
                    data = recv_msg(connection)
                    logging.info('received "%s"', data)
                    if data:
                        if self.parse_data(data):
                            if self.on_action():
                                logging.info(self._action_out)
                                send_msg(connection,self._action_out)
                        else:
                            send_msg(connection, "Invalid command!\n")
                    else:
                        logging.info("no more data from '%s'", self._client_address)
                        break
            except socket.error as err:
		print "socket error happens!"
            finally:
                # Clean up the connection
                print "connection closed!"
                connection.close()

    def on_new_connection(self):
        #do some initialization things
        logging.info("A new connection has been started!")

    def parse_data(self, data):
        logging.info("Try to parse '%s'", data)
        if len(data) == 0:
            logging.error("Incorrect command '%s'", data)
            self.cmd_str = ''
            self.cmd_type ='invalid'
            return False

	if data == "hb": #heart beat message
            self.cmd_str = ""
            self.cmd_type = "hb"
            return True
        if data[0:5] == "[raw]":
            cmd_str = data[5:]
            #store the argument list
            self.cmd_str = cmd_str
            self.cmd_type = 'raw';
            return True
        elif len(data) >= 9 and data[0:9] == 'record_on':
            #get the topic names that are required to be record
            cmd_str = "rosbag record " + data[9:]
            self.cmd_str = cmd_str
            self.cmd_type = 'record_on'
            return True
        elif len(data) >= 10 and data[0:10] == 'record_off':
            self.cmd_str = ''
            self.cmd_type = 'record_off'
            return True
        elif len(data) >= 12 and data[0:12] == 'streaming_on':
            argv_list = data.split()
            if len(argv_list) > 1:
                return False
            self.cmd_str = ''
            self.cmd_type = 'streaming_on'
            return True
        elif len(data) >= 12 and data[0:13] == 'streaming_off':
            argv_list = data.split()
            if len(argv_list) > 1:
                return False
            self.cmd_str = ''
            self.cmd_type = 'streaming_off'
            return True
        elif data == 'list_ros_topics':
            self.cmd_str = ''
            self.cmd_type = 'list_ros_topics'
            return True
        elif data == 'switch_camera forward':
            self.cmd_str = 'forward'
            self.cmd_type = 'switch_camera'
            return True
        elif data == 'switch_camera downward':
            self.cmd_str = 'downward'
            self.cmd_type = 'switch_camera'
            return True

        self.cmd_str = ''
        self.cmd_type = 'invalid'
        return False


    def on_action(self):
        if self.cmd_type == 'invalid':
            return False
        try:
            self._action_out = ''
            self._action_err = ''
            if self.cmd_type == 'raw':
                out,err = run_raw_command(self.cmd_str)
                self._action_out = out
                self._action_err = err
                if err <> "":
                    return False
            elif self.cmd_type == "hb":
                self._action_out ="ae"
                return True
            elif self.cmd_type == 'record_on':
                if self.record_bag_on():
                    self._action_out = "record_on OK!"
                    return True
            elif self.cmd_type == 'record_off':
                if self.record_bag_off():
                    self._action_out = "record_off OK!"
                    return True
            elif self.cmd_type == 'streaming_on':
                if self.streaming_on():
                    self._action_out = "streaming_on OK!"
                    return True
            elif self.cmd_type == 'streaming_off':
                if self.streaming_off():
                    self._action_out = "streaming_off OK!"
                    return True
            elif self.cmd_type == 'switch_camera':
                if self.switch_camera(self.cmd_str):
                    self._action_out = "switch_camera OK!"
                    return True
            elif self.cmd_type == 'list_ros_topics':
                if self.list_ros_topics():
                    return True
            else:
                return False

        except Exception, e:
            logging.error(e)
            return False

        return True

    def record_bag_on(self):
        if self._is_recording:
            logging.error("rosbag is already recording. Please stop the recording first")
            return False
        else:
            subprocess.Popen(self.cmd_str.split())
            #sleep for 200ms to wait for the update of the node list
            time.sleep(0.2)
            out,err = run_raw_command("rosnode list")
            self._record_topic='';
            topic_list = out.split()
            for name in topic_list:
                if len(name) == 27 and name[0:8] == '/record_':
                    self._record_topic = name
                    self._is_recording = True
                    return True
            if not self._record_topic:
                return False
        return True

    def record_bag_off(self):
        if self._is_recording:
            out,err = run_raw_command("rosnode list")
            if len(err) > 0:
                logging.error(err)
                return False

            ##find the record_* topics, and kill them altogether
            topic_list = out.split()
            for name in topic_list:
                if len(name) == 27 and name[0:8] == '/record_':
                    #send rosnode kill command to stop recording
                    run_raw_command("rosnode kill "+name)

            #check if there are any 'record_' topics
            out,err = run_raw_command("rosnode list")
            if len(err) > 0:
                logging.error(err)
                return False
            topic_list = out.split()
            for name in topic_list:
                if len(name) == 27 and name[0:8] == '/record_':
                    return False
            self._is_recording = False
        return True


    def check_streaming_on(self):
        out,err = run_raw_command("rosnode list")
        topic_list = out.split()
        for name in topic_list:
            if name == "/camera_streaming":
                return True
        return False


    def streaming_on(self):
        if self.check_streaming_on():
            logging.error("It is already streaming. Please stop the streaming first")
            return False
	#just for debug
        #out,err = run_raw_command("rosparam set /camera_streaming/topic_name /v4l/camera/image_raw")
	out,err = run_raw_command(self._streaming_str)
        if len(err) > 0:
            return False
        out,err = run_raw_command("rosparam set /camera_streaming/host %s"%self._client_address[0])
        if len(err) > 0:
            return False

        subprocess.Popen("rosrun camera_streaming camera_streaming_node".split())
        time.sleep(0.2)
        if not self.check_streaming_on():
            logging.error("fail in 'rosrun camera_streaming camera_streaming_node'")
            return False
        return True

    def streaming_off(self):
        if self.check_streaming_on():
            out,err = run_raw_command('rosnode kill /camera_streaming')
            if len(err) > 0:
                return False
        return True

    def switch_camera(self,cam_str):
        if cam_str == "forward":
            self._streaming_str = "rosparam set /camera_streaming/topic_name /camera/color/image_raw"
        elif cam_str == "downward":
            self._streaming_str = "rosparam set /camera_streaming/topic_name /aero/bcam/image"            
        else:
            return False
        if not self.streaming_off():
            return False
        if not self.streaming_on():
            return False
        return True
    
    def list_ros_topics(self):
        out,err = run_raw_command("rostopic list")
        if len(err) > 0:
            return False
        self._action_out = out
        return True


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG)
    if (not check_roscore()):
        logging.error("ROS core has not been detected. Quit!")
        sys.exit(-1)

    aero_server = AeroServer(10000)
    try:
        aero_server.start()
    except Exception, e:
        logging.error(e)
    finally:
        aero_server._sock.close()
