from server_main import *

def test_rosbag_record_on():
    print "hello"
    aero_server = AeroServer(100000)
    aero_server._is_recording = False
    aero_server.cmd_str = "rosbag record /aero/bcam/image"
    if aero_server.record_bag_on():
        print "rosbag recording has been successfully started"
        print "The topic of recording is '%s'" % aero_server._record_topic


def test_rosbag_record_off():
    aero_server = AeroServer(100000)
    aero_server._is_recording = True
    if not aero_server.record_bag_off():
        print "cannot stop the rosbag recording!"
    else:
        print "succeed in stopping the rosbag recording."


def test_check_roscore():
    if check_roscore():
        print "ROS has been started!"
    else:
        print "ROS has not been started!"


def test_parse_data_action():
    aero_server = AeroServer(100000)
    cmd_str_list = ["[raw]rosnode list\n",
                    "sldkfjlsdfj\n",
                    "[raw]rosparam set\n"
                    "sfdf\n",
                    "rosbag\n",
                    "record_on /aero/bcam/image\n",
                    "record_off\n"]

    for cmd in cmd_str_list:
        print "cmd-'%s'"%cmd
        print "parse_data:%d" % aero_server.parse_data(cmd)
        print "on_action:%d" % aero_server.on_action()
        print aero_server._action_out
        print aero_server._action_err



def test_streaming_on():
    aero_server = AeroServer(100000)
    aero_server._client_address = ('192.168.1.5','40360')
    aero_server.streaming_on()

def test_streaming_off():
    aero_server = AeroServer(100000)
    aero_server.streaming_off()

if __name__ == "__main__":
    #test_check_roscore()
    #test_rosbag_record_on()
    #test_rosbag_record_off()
    #test_parse_data_action()
    #test_streaming_on()
    test_streaming_off()

