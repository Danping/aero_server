#!/bin/bash
source /opt/ros/kinetic/setup.bash
source /home/aero/catkin_ws/devel/setup.bash
source /home/aero/Src/ros_ws/devel/setup.bash
echo "start r200 (realsense)"
roslaunch realsense_camera r200_nodelet_default.launch 2>&1 >/dev/null &
sleep 2
echo "start mavros(px4)"
roslaunch mavros px4.launch 2>&1 >/dev/null &
sleep 2
echo "start aero_sensors (for bottom camera)"
roslaunch aero_sensors start.launch 2>&1 >/dev/null &
sleep 2
echo "start aero_server (for ground station)"
python /home/aero/python_scripts/aero_server/server_main.py
